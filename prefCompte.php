<!DOCTYPE html>
<html lang="fr">
    <?php
        session_start();
        include "fonctionDB.php";
        //identification function
        $connexion = connect();
        sessionConnexion($connexion);
        
          if (isset($_POST['btnpref'])){
              $newmdp=$_POST['newmdp']; //Création du mot de passe
              if (md5($_POST['oldmdp'])!=$_SESSION['password']){
		      $msg="Votre mot de passe n'est pas bon."; 
		      //message à afficher si le mot de passe est faux
              } 
              elseif ($_POST['newmdp']!=$_POST['newmdp2']){
                  $msg="Les nouveaux mots de passe ne correspondent pas.";
              } 
              else {
                  $login=$_SESSION['login'];
                  $mdp=$_SESSION['password'];
                  
                  changeMDP($connexion,$newmdp,$login,$mdp);
              }
          }
    ?>

    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="MSDN LLB Keys">
    <meta name="author" content="LLB">
    <link rel="shortcut icon" href="favicon.ico">
    <title>MSKeys LLB</title>   //Titre
    
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">    //Link du Framework css
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    
    <!--Bootstrap core JavaScript-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">MSKeys LLB</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Accueil</a></li>
                <li ><a href="importations.php">Importations</a></li>
		<li ><a href="gestOS.php">Gestion d'OS</a></li>
		<li><a href="statistiques.php">Statistiques</a></li>
                <li class="active" ><a href="prefCompte.php">Paramètres</a></li>
            </ul>
            <form class="navbar-form navbar-right" role="form" action="index.php" method="post">
                <input class="btn btn-warning" name="logout" type="submit" value="Déconnexion"></input>
            </form>
        </div><!-- /.navbar-collapse -->
    </nav>
      <div class="container"><br><br><br>
          <?php
          //form to change password
          echo '<h3><center>'.$msg.'</center></h3>'; ?>
          <form class="form-inline" method="POST" action="prefCompte.php">
          <center><div class="input-group"><br>
          <p>Saisissez votre ancien mot de passe: <br>
          <input class="form-control" type="password" name="oldmdp" required /></p><br> //saisir l'ancien mot de passe pour pouvoir mle réinitialiser
          <p>Saisissez votre nouveau mot de passe: <br>
	      <input class="form-control" type="password" name="newmdp" required /></p><br> // saisir le nouveau mot de passe
          <p>Saisissez à nouveau votre mot de passe: <br>
              <input class="form-control" type="password" name="newmdp2" required /></p><br> // confirmer le nouveau mot de passe
          <input class="btn btn-info" type="submit" name="btnpref" value="Valider"/>
              </div>
          </center>
      </form>  
  </body>
