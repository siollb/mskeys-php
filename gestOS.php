<!DOCTYPE html>
<html lang="fr">
    <?php
        session_start();
        include "fonctionDB.php";
         
        $connexion = connect();
        sessionConnexion($connexion);
        $tab_os=recupOS($connexion);
        
    ?>

    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="MSDN LLB Keys">
    <meta name="author" content="LLB">
    <link rel="shortcut icon" href="favicon.ico">
    <title>MSKeys LLB</title>
    
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    
    <!--Bootstrap core JavaScript-->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>
  <body>
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">MSKeys LLB</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Accueil</a></li>
                <li ><a href="importations.php">Importations</a></li>
                <li class="active"><a href="gestOS.php">Gestion d'OS</a></li>
		<li><a href="statistiques.php">Statistiques</a></li>
                <li ><a href="prefCompte.php">Paramètres</a></li>
            </ul>
            <form class="navbar-form navbar-right" role="form" action="index.php" method="post">
                <input class="btn btn-warning" name="logout" type="submit" value="Déconnexion"></input>
            </form>
        </div><!-- /.navbar-collapse -->
    </nav>
      <div class="jumbotron">
          <?php if (($_SESSION['login']) AND ($_SESSION['password'])){ ?>
          <div class="btn-group">
              <a href="formulaireOS.php?action=ajout"> <button type="button"  class="btn btn-danger">Ajouter</button></a>       
          </div>
        <center>
          
            <?php
                        foreach($tab_os as $os)
                        {
                            $id=$os['idProduct'];
                            $nom=$os['name'];
                            echo $nom."\t \t";
                            echo '<div class="btn-group">';
                            echo '<button type="button" class="btn btn-danger">Action</button>';
                            echo '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">';
                            echo '<span class="caret"></span>';
                            echo '<span class="sr-only">Toggle Dropdown</span>';
                            echo '</button>';
                            echo '<ul class="dropdown-menu" role="menu">';
                            echo "<li><a href='formulaireOS.php?id=$id&nom=$nom&action=modif'>Editer</a></li>";
                            echo '<li class="divider"></li>';
                            echo "<li><a href='actionOS.php?id=$id&action=suppr'>Supprimer</a></li>";
                            echo '</ul>'; 
                            echo '</div>';
                            echo '<br>';
                        }
                        ?>
            
        </center>
          <?php
            }
            else {?>
                <div class="container">
                    <h2>Bienvenue</h2>
                    <h4>Merci de vous authentifier pour acc&eacute;der &agrave; l'application de gestion des licences Microsoft du lyc&eacute;e Laetitia Bonaparte.</h4>
<br />
<br />
<br />
<br />
<br />
                </div><?php
            }?>
      </div>
  </body>
