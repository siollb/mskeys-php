MSKeys-LLB_dev
==========

Version en développement de l'application web permettant la gestion des licences Windows au sein du lycee Laetitia Bonaparte.


Rappel du besoin :
Le lycée Laetitia Bonaparte dispose d’un abonnement MSDN (MicroSoft Developper Network) et bénéficie ainsi de licences pour de nombreux outils Microsoft comme notamment les systèmes d’exploitation Windows 7, Windows 8 et Windows Server 2008.
Ces licences sont téléchargeables sous forme de lot au format XML.
Aucune gestion de ces licences n’est proposée sur l’interface MSDN, il n’est donc pas possible de savoir quelles sont les clés utilisées et donc par conséquent lesquelles sont disponibles.
L’équipe informatique du lycée souhaite disposer d’une application Web permettant une gestion des licences Microsoft.

L'application en production est accessible à l'adresse http://llb.ac-corse.fr/mskeys

Environnement de Développement Intégré (IDE):
Netbeans

La base de données:
MySQL 

Langage de programmation:
HTML5, CSS3, JS, PHP5

Frameworks:
Bootstrap 3,
jQuery 2.0.3

Statistique.php sert a gerer les statistique
