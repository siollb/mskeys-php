/* Debut du code*/
<?php
    session_start();
    include "fonctionDB.php";
    /*connexion à la base de données*/
    $connexion = connect();
    /*démarrage de la session*/
    sessionConnexion($connexion);
    /*recuperation de l'action depuis les parametres GET*/    
    $action = $_GET['action'];
/*verification de l'existence  de  l'identifiant lucca*/
    if(isset($_GET['id'])){
        $id=$_GET['id'];
    }
    /*verification de l'existence  du nom */
    if(isset($_POST['nom']))
        $nom=$_POST['nom'];
        
    /*execution d'une action en fonction de la valeur  de $action*/
    switch($action)
    {
        case'modif':
            modifOS($connexion,$id,$nom);
            break;
        case 'suppr':
            supprimOS($connexion, $id);
            break;
        case 'ajout':
            ajoutOS($connexion,$nom);
            break;
    }
     /*redirection vers la pages gestoS.php*/
    header("Location: gestOS.php");
        
?>
